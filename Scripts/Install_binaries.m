%% Version checking and Get remote Binaries

tic
% warning settings
warning('on','all')
warning('off','backtrace')

try
    Install_local_binaries
catch
    warning('local version of "MathWorks_Motorsport_Binaries" folder not found or currupt')
end

try 
    Install_remote_binaries
catch
    warning('Seems like something went wrong when trying to download Binary file - Please manually download "MathWorks_Motorsport_Binaries.zip" using this link - https://drive.google.com/file/d/1cVB2wfIqaF0TEaya_87-c_MCK6fRs3vl/view')
    warning('After downloading place the file in the project root extract the file into the project root directory this should create an unzipped folder named MathWorks_Motorsport_Binaries" once this is complete, close and open project again')
end
toc