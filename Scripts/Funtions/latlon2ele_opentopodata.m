function [lat_v,lon_v,ele_v] = latlon2ele_opentopodata(lat_lookup_s,lat_lookup_e,lon_lookup_s,lon_lookup_e,N_lat,N_lon,Dataset,pause_factor)


% lat_lookup_s = -26;
% lat_lookup_e = -25.95;
% lon_lookup_s = 28.5;
% lon_lookup_e = 28.55;
% N_lat = 10; %100000 max
% N_lon = 10; %max 100 for opentopodata_surfread
% Dataset = 'aster30m';% other options - 'aster30m' 'etopo1' 'eudem25m' 'mapzen' 'ned10m' 'nzdem8m' 'srtm30m' 'emod2018' 'gebco2020' 'bkg200m'


lat_lookup = linspace(lat_lookup_s,lat_lookup_e,N_lat);
lon_lookup = linspace(lon_lookup_s,lon_lookup_e,N_lon);

lon_lookup_s = [lon_lookup(N_lon/5*0+1) lon_lookup(N_lon/5*1+1) lon_lookup(N_lon/5*2+1) lon_lookup(N_lon/5*3+1) lon_lookup(N_lon/5*4+1)];
lon_lookup_e = [lon_lookup(N_lon/5*1) lon_lookup(N_lon/5*2) lon_lookup(N_lon/5*3) lon_lookup(N_lon/5*4) lon_lookup(N_lon/5*5)];


[lat_v,lon_v,ele_v] = opentopodata_surfread(lat_lookup(1),lon_lookup_s(1),lon_lookup_e(1),Dataset,N_lon/5);
pause((100-(N_lon/5))/100*pause_factor);
for c = 2:N_lat%N_lat
[lat_vt,lon_vt,ele_vt] = opentopodata_surfread(lat_lookup(c),lon_lookup_s(1),lon_lookup_e(1),Dataset,N_lon/5);
lat_v = cat(1,lat_v,lat_vt);
lon_v = cat(1,lon_v,lon_vt);
ele_v = cat(1,ele_v,ele_vt);
pause((100-(N_lon/5))/100*pause_factor);
end
pause((100-(N_lon/5))/100*pause_factor);
for c = 1:N_lat%N_lat
[lat_vt,lon_vt,ele_vt] = opentopodata_surfread(lat_lookup(c),lon_lookup_s(2),lon_lookup_e(2),Dataset,N_lon/5);
lat_v = cat(1,lat_v,lat_vt);
lon_v = cat(1,lon_v,lon_vt);
ele_v = cat(1,ele_v,ele_vt);
pause((100-(N_lon/5))/100*pause_factor);
end
pause((100-(N_lon/5))/100*pause_factor);
for c = 1:N_lat%N_lat
[lat_vt,lon_vt,ele_vt] = opentopodata_surfread(lat_lookup(c),lon_lookup_s(3),lon_lookup_e(3),Dataset,N_lon/5);
lat_v = cat(1,lat_v,lat_vt);
lon_v = cat(1,lon_v,lon_vt);
ele_v = cat(1,ele_v,ele_vt);
pause((100-(N_lon/5))/100*pause_factor);
end
pause((100-(N_lon/5))/100*pause_factor);
for c = 1:N_lat%N_lat
[lat_vt,lon_vt,ele_vt] = opentopodata_surfread(lat_lookup(c),lon_lookup_s(4),lon_lookup_e(4),Dataset,N_lon/5);
lat_v = cat(1,lat_v,lat_vt);
lon_v = cat(1,lon_v,lon_vt);
ele_v = cat(1,ele_v,ele_vt);
pause((100-(N_lon/5))/100*pause_factor);
end
pause((100-(N_lon/5))/100*pause_factor);
for c = 1:N_lat%N_lat
[lat_vt,lon_vt,ele_vt] = opentopodata_surfread(lat_lookup(c),lon_lookup_s(5),lon_lookup_e(5),Dataset,N_lon/5);
lat_v = cat(1,lat_v,lat_vt);
lon_v = cat(1,lon_v,lon_vt);
ele_v = cat(1,ele_v,ele_vt);
pause((100-(N_lon/5))/100*pause_factor);
end


end