% function [lat_v,lon_v,ele_v] = latlon2ele_opentopodata(lat_lookup_s,lat_lookup_e,lon_lookup_s,lon_lookup_e,N_lat,N_lon,Dataset,pause_factor)
function [x_adj, y_adj] = Create_RRHD_abs(direction, Terrain, Track,Nfrom_end,reduce_res,resampleFactor)
y_adj = (max(Terrain.local.y.Data)-min(Terrain.local.y.Data))/2;
x_adj = (max(Terrain.local.x.Data)-min(Terrain.local.x.Data))/2;

y_adj = y_adj - (mean(Terrain.local.y.Data)-mean(Terrain.local.y_b.Data));
x_adj = x_adj - (mean(Terrain.local.x.Data)-mean(Terrain.local.x_b.Data));


if direction == "anti-clockwise"
% assuming anti-clockwise driving, left is inner side and right is outer
% side for Track
    lftBndry = [Track.local.x_i.Data(1:end-Nfrom_end)'-x_adj Track.local.y_i.Data(1:end-Nfrom_end)'-y_adj Track.local.z_i_abs.Data(1:end-Nfrom_end)'];
    ctrBndry = [Track.local.x_c.Data(1:end-Nfrom_end)'-x_adj Track.local.y_c.Data(1:end-Nfrom_end)'-y_adj Track.local.z_c_abs.Data(1:end-Nfrom_end)'];
    rgtBndry = [Track.local.x_o.Data(1:end-Nfrom_end)'-x_adj Track.local.y_o.Data(1:end-Nfrom_end)'-y_adj Track.local.z_o_abs.Data(1:end-Nfrom_end)'];
else
    rgtBndry = [Track.local.x_i.Data(1:end-Nfrom_end)'-x_adj Track.local.y_i.Data(1:end-Nfrom_end)'-y_adj Track.local.z_i_abs.Data(1:end-Nfrom_end)'];
    ctrBndry = [Track.local.x_c.Data(1:end-Nfrom_end)'-x_adj Track.local.y_c.Data(1:end-Nfrom_end)'-y_adj Track.local.z_c_abs.Data(1:end-Nfrom_end)'];
    lftBndry = [Track.local.x_o.Data(1:end-Nfrom_end)'-x_adj Track.local.y_o.Data(1:end-Nfrom_end)'-y_adj Track.local.z_o_abs.Data(1:end-Nfrom_end)'];
end

if reduce_res == 0
ctrBndry(end,:) = ctrBndry(1,:);
lftBndry(end,:) = lftBndry(1,:);
rgtBndry(end,:) = rgtBndry(1,:);
end



% reduce_res = 1;
% resampleFactor = downsamp;
indices_orig = 1:size(lftBndry,1);
indices_resampled = 1:resampleFactor:size(lftBndry,1);

if reduce_res==1
    lftBndry = interp1(indices_orig,lftBndry,indices_resampled);
    ctrBndry = interp1(indices_orig,ctrBndry,indices_resampled);
    rgtBndry = interp1(indices_orig,rgtBndry,indices_resampled);
end

% Create an empty RoadRunner HD Map
rrMap = roadrunnerHDMap();

% Pre-initialize for better performance as the number of objects in the map increases
rrMap.Lanes(1,1) = roadrunner.hdmap.Lane();
rrMap.LaneBoundaries(2,1) = roadrunner.hdmap.LaneBoundary();

% Assign Lane property values. Use deal to match up the input and output lists.
[rrMap.Lanes.ID] = deal("Lane1");
[rrMap.Lanes.TravelDirection] = deal("Forward");
[rrMap.Lanes.LaneType] = deal("Driving");

% Average data for lane boundaries to obtain lane centers
lftLane = lftBndry;
rgtLane = rgtBndry;
[rrMap.Lanes.Geometry] = deal(ctrBndry);

% The center lane boundary is shared between lanes "Lane1" and "Lane2"
[rrMap.LaneBoundaries.ID] = deal("Left","Right");
[rrMap.LaneBoundaries.Geometry] = deal(lftBndry,rgtBndry);

% Associate lane boundaries to lane using their ID
leftBoundary(rrMap.Lanes(1),"Left",Alignment="Forward");
rightBoundary(rrMap.Lanes(1),"Right",Alignment="Forward");


% Define path to a solid white lane marking asset
solidWhiteAsset = roadrunner.hdmap.RelativeAssetPath(AssetPath="Assets/Markings/SolidSingleWhite.rrlms");

% Add a solid double yellow marking in addition to the solid white marking
doubleYellowAsset = roadrunner.hdmap.RelativeAssetPath(AssetPath="Assets/Markings/SolidDoubleYellow.rrlms");

rrMap.LaneMarkings(1,1) = roadrunner.hdmap.LaneMarking();
[rrMap.LaneMarkings.ID] = deal("SolidWhite");
[rrMap.LaneMarkings.AssetPath] = deal(solidWhiteAsset);

% Assign white marking to the lane boundaries at lane edges and yellow marking to the center
% lane boundary. The markings span the entire length of the boundary.
markingSpan = [0 1];
markingRefSY = roadrunner.hdmap.MarkingReference(MarkingID=roadrunner.hdmap.Reference(ID="DoubleYellow"));
markingAttribSY = roadrunner.hdmap.ParametricAttribution(MarkingReference=markingRefSY,Span=markingSpan);

% Create a reference for the solid white marking to apply to lane boundaries
markingRefSW = roadrunner.hdmap.MarkingReference(MarkingID=roadrunner.hdmap.Reference(ID="SolidWhite"));
markingAttribSW = roadrunner.hdmap.ParametricAttribution(MarkingReference=markingRefSW,Span=markingSpan);
[rrMap.LaneBoundaries.ParametricAttributes] = deal(markingAttribSW,markingAttribSW);
%%
% Set the geographic bounds for the map
geoBounds = [min([ctrBndry; rgtBndry ;lftBndry]); max([ctrBndry; rgtBndry ;lftBndry])];
rrMap.GeographicBoundary = geoBounds;

% Set the geographic reference
% latitude_grid(1)+(max(latitude_grid)-min(latitude_grid))/2

% rrMap.GeoReference = [(max(Track.lat_lon_ele_Dis.lat_center.Data(1,:))+min(Track.lat_lon_ele_Dis.lat_center.Data(1,:)))/2 (max(Track.lat_lon_ele_Dis.lon_center.Data(1,:))+min(Track.lat_lon_ele_Dis.lon_center.Data(1,:)))/2];
rrMap.GeoReference = [(max(Terrain.lat_lon_ele.latitude_grid.Data)+min(Terrain.lat_lon_ele.latitude_grid.Data))/2 (max(Terrain.lat_lon_ele.longitude_grid.Data)+min(Terrain.lat_lon_ele.longitude_grid.Data))/2];



% Change file name
name = sprintf('%s',Track.local.trackName,'_RR_Track_abs'); 
filename = strcat(name,'.rrhd');
Destination = strcat('Output\',filename);

% Write the map to a file
write(rrMap,filename);
movefile(filename,Destination)
end