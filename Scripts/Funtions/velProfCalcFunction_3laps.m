% Install following Add-On for curvaure function to work
% https://in.mathworks.com/matlabcentral/fileexchange/69452-curvature-of-a-1d-curve-in-a-2d-or-3d-space 
%% Data form

% INPUT DATA
% name = 'name_of_track'
% m = mass
% atmax = max traction (m/s^2)
% abmax = max braking (m/s^2)
% anmax = max cornering (m/s^2)
% trackData - [x-ref y-ref xin yin xout yout]
% Values I had taken
% m = 740, atmax = 16, abmax = -18, anmax = 30

% OUTPUT DATA
% velProf = velocity profile of the given track

function [velProf1,velProf2,velProf3,time1,time2,time3] = velProfCalcFunction_3laps(traj,name,m,atmax,abmax,anmax,trackData,max_vel,len,N_track)
%% Initialization



x = traj(:,1);
y= traj(:,2);
n = numel(x);
accel = zeros(size(x)); % acceleration profile
decel = zeros(size(x)); % deceleration profile
ftmax = atmax*m; % traction max
fbmax = -abmax*m; % braking max
fnmax = anmax*m; % cornering max
drag = 0.0021*m; % drag


%% Curvature

[~,R,~] = curvature([x y]);
K = 1./R;
% K(1) = K(2);
% K(end) = K(1);
% K(end-1) = K(1);

% K(1:2) = 0;
% K(end-1:end) = 0;
% 
% SF = floor(length(K)/2);
% K(SF-2:SF+2) = 0;
% clear SF

K(isnan(K)) = 0; %Replace NaN with 0
K(K<3e-3) = 0; %ignore small Curvatures


% K((length(K)+1)/2) = 0;
% K(((length(K)+1)/2)+1) = 0;
% 
% K((length(K)-1)/2) = 0;
% K(((length(K)-1)/2)-1 = 0;


[~,locs] = findpeaks(K);

%% Velocity Profile Calculation

% start section
accel(1) = 0;
for i = 2:locs(1)
     accel(i) = sqrt(2*sqrt((ftmax-drag*accel(i-1)^2)^2 ...
                            -(m*K(i-1)*((ftmax-drag*accel(i-1)^2)/fnmax)^2*accel(i-1)^2)^2)*(len(i)-len(i-1))/m ...
                            + accel(i-1)^2);
end
    
for i = 1:numel(locs)
    % check if accel vel is greater than crit vel
    vcrit = sqrt(fnmax/(m*K(locs(i))));
    if accel(locs(i)) < vcrit
        curVel = accel(locs(i));
    else
        curVel = vcrit;
        accel(locs(i)) = vcrit;
    end

    % next step using cur vel
    accel(locs(i)+1) = sqrt(2*sqrt((ftmax-drag*curVel^2)^2 ...
                                    -(m*K(locs(i)-1)*((ftmax-drag*curVel^2)/fnmax)^2*curVel^2)^2)*(len(locs(i))-len(locs(i)-1))/m ...
                                    + curVel^2);
    
    if i == numel(locs)
        % end section
        for j = locs(i)+2:n
            accel(j) = sqrt(2*sqrt((ftmax-drag*accel(i-1)^2)^2 ...
                                    -(m*K(j-1)*((ftmax-drag*accel(i-1)^2)/fnmax)^2*accel(j-1)^2)^2)*(len(j)-len(j-1))/m ...
                                    + accel(j-1)^2);
        end
    else
        % usual step
        for j = locs(i)+2:locs(i+1)
            accel(j) = sqrt(2*sqrt((ftmax-drag*accel(j-1)^2)^2 ...
                                    -(m*K(j-1)*((ftmax-drag*accel(j-1)^2)/fnmax)^2*accel(j-1)^2)^2)*(len(j)-len(j-1))/m ...
                                    + accel(j-1)^2);
        end
    end
end

% end point
decel(end) = 200; % max final velocity 
for j = numel(decel)-1:-1:locs(end)
    decel(j) = sqrt(2*sqrt((fbmax-drag*decel(j+1)^2)^2 ...
                            -(m*K(j+1)*((fbmax-drag*decel(j+1)^2)/fnmax)^2*decel(j+1)^2)^2)*(len(j+1)-len(j))/m ...
                            + decel(j+1)^2);
end

for i = numel(locs):-1:1
    % check if decel vel is greater than crit vel
    vcrit = sqrt(fnmax/(m*K(locs(i))));
    if decel(locs(i)) < vcrit
        curVel = decel(locs(i));
    else
        curVel = vcrit;
        decel(locs(i)) = vcrit;
    end
    % next step using cur vel
    decel(locs(i)-1) = sqrt(2*sqrt((fbmax-drag*curVel^2)^2 ...
                                    -(m*K(locs(i))*((fbmax-drag*curVel^2)/fnmax)^2*curVel^2)^2)*(len(locs(i))-len(locs(i)-1))/m ...
                                    + curVel^2);

    if i == 1
        % start section
        for j = locs(i)-2:-1:1
            decel(j) = sqrt(2*sqrt((fbmax-drag*decel(j+1)^2)^2 ...
                                    -(m*K(j+1)*((fbmax-drag*decel(j+1)^2)/fnmax)^2*decel(j+1)^2)^2)*(len(j+1)-len(j))/m ...
                                    + decel(j+1)^2);
        end
    else
        % usual step
        for j = locs(i)-2:-1:locs(i-1)
            decel(j) = sqrt(2*sqrt((fbmax-drag*decel(j+1)^2)^2 ...
                                    -(m*K(j+1)*((fbmax-drag*decel(j+1)^2)/fnmax)^2*decel(j+1)^2)^2)*(len(j+1)-len(j))/m ...
                                    + decel(j+1)^2);
        end
    end
end

velProf = min(accel,decel);
velProf = abs(velProf);

velProf_unconstraind = velProf;
velProf_t = velProf;
velProf(velProf>max_vel) = max_vel; % Determine where the velocity profile is faster than what the velicle can achieve and set to max velocity of vehicle 

for nn = 1:length(velProf_t)                          % add small random number to aid in velocity calc
    if velProf_t(nn) >= max_vel
        velProf_t(nn) = max_vel+(rand(1)-0.5)/10;
    end
end


%% Lap Time calcluation

timeA = zeros(size(x));
for i = 2:numel(x)
    acc = (velProf_t(i)^2-velProf_t(i-1)^2)/(2*(len(i)-len(i-1)));
    timeA(i) = timeA(i-1) + (velProf_t(i)-velProf_t(i-1))/acc;
end

%% Split into the three lap types: First, Middle and Last

time1 = timeA(1:N_track);
time2 = timeA(N_track:N_track*2-1);
time3 = timeA(N_track*2-1:N_track*3-2);
time1 = time1 - time1(1);
time2 = time2 - time2(1);
time3 = time3 - time3(1);

velProf_unconstraind1 = velProf_unconstraind(1:N_track);
velProf_unconstraind2 = velProf_unconstraind(N_track:N_track*2-1);
velProf_unconstraind3 = velProf_unconstraind(N_track*2-1:N_track*3-2);

velProf1 = velProf(1:N_track);
velProf2 = velProf(N_track:N_track*2-1);
velProf3 = velProf(N_track*2-1:N_track*3-2);

len123 = len(1:N_track);



%% Three sets of figures (1)
%% Plot Velocity Profile (v vs s)

figure
hold on
plot(len123,velProf_unconstraind1*3.6,'r','LineWidth',2)
plot(len123,velProf1*3.6,'b','LineWidth',2)

grid on
xlabel('s(m)','fontweight','bold','fontsize',14)
ylabel('kmph','fontweight','bold','fontsize',14)
title(sprintf('%s - 1st-lap Velocity Profile\nLap Time = %.2fs',name,time1(end)),'fontsize',16)

%% Plot velocity profile onto trajectory

figure
plot(traj(:,1),traj(:,2),'color','w','linew',1)
hold on

% plot starting line
plot([trackData(1,3) trackData(1,5)],[trackData(1,4) trackData(1,6)], 'color','b','linew',2)
% plot([xin(2) xout(2)], [yin(2) yout(2)],'color','k','linew',2)

% plot reference line
plot(trackData(:,1),trackData(:,2),'--')
hold on

% plot inner track
plot(trackData(:,3),trackData(:,4),'color','k','Linewidth',0.2)

%plot outer track
plot(trackData(:,5),trackData(:,6),'color','k','Linewidth',0.2)

scatter(traj(1:length(velProf1),1),traj(1:length(velProf1),2),5,velProf1,'filled')

R = linspace(1,0,256);
G = linspace(0,1,256);
B = zeros(1,256);
map = [R' G' B'];
colormap(map);

colBar = colorbar('eastoutside');
colBar.Label.String = "Velocity [m/s]";
xlabel('X [m]');
ylabel('Y [m]');
axis equal;
hold off;

xlabel('x(m)','fontweight','bold','fontsize',14)
ylabel('y(m)','fontweight','bold','fontsize',14)
title(sprintf('%s - 1st-Lap Velocity Profile\nLap Time = %.2fs',name,time1(end)),'fontsize',16)
%% Three sets of figures (2)
%% Plot Velocity Profile (v vs s)

figure
hold on
plot(len123,velProf_unconstraind2*3.6,'r','LineWidth',2)
plot(len123,velProf2*3.6,'b','LineWidth',2)

grid on
xlabel('s(m)','fontweight','bold','fontsize',14)
ylabel('kmph','fontweight','bold','fontsize',14)
title(sprintf('%s - Middle-laps Velocity Profile\nLap Time = %.2fs',name,time2(end)),'fontsize',16)

%% Plot velocity profile onto trajectory

figure
plot(traj(:,1),traj(:,2),'color','w','linew',1)
hold on

% plot starting line
plot([trackData(1,3) trackData(1,5)],[trackData(1,4) trackData(1,6)], 'color','b','linew',2)
% plot([xin(2) xout(2)], [yin(2) yout(2)],'color','k','linew',2)

% plot reference line
plot(trackData(:,1),trackData(:,2),'--')
hold on

% plot inner track
plot(trackData(:,3),trackData(:,4),'color','k','Linewidth',0.2)

%plot outer track
plot(trackData(:,5),trackData(:,6),'color','k','Linewidth',0.2)

scatter(traj(1:length(velProf2),1),traj(1:length(velProf2),2),5,velProf2,'filled')

R = linspace(1,0,256);
G = linspace(0,1,256);
B = zeros(1,256);
map = [R' G' B'];
colormap(map);

colBar = colorbar('eastoutside');
colBar.Label.String = "Velocity [m/s]";
xlabel('X [m]');
ylabel('Y [m]');
axis equal;
hold off;

xlabel('x(m)','fontweight','bold','fontsize',14)
ylabel('y(m)','fontweight','bold','fontsize',14)
title(sprintf('%s - Middle-Laps Velocity Profile\nLap Time = %.2fs',name,time2(end)),'fontsize',16)
%% Three sets of figures (3)
%% Plot Velocity Profile (v vs s)

figure
hold on
plot(len123,velProf_unconstraind3*3.6,'r','LineWidth',2)
plot(len123,velProf3*3.6,'b','LineWidth',2)

grid on
xlabel('s(m)','fontweight','bold','fontsize',14)
ylabel('kmph','fontweight','bold','fontsize',14)
title(sprintf('%s - Last-lap Velocity Profile\nLap Time = %.2fs',name,time3(end)),'fontsize',16)

%% Plot velocity profile onto trajectory

figure
plot(traj(:,1),traj(:,2),'color','w','linew',1)
hold on

% plot starting line
plot([trackData(1,3) trackData(1,5)],[trackData(1,4) trackData(1,6)], 'color','b','linew',2)
% plot([xin(2) xout(2)], [yin(2) yout(2)],'color','k','linew',2)

% plot reference line
plot(trackData(:,1),trackData(:,2),'--')
hold on

% plot inner track
plot(trackData(:,3),trackData(:,4),'color','k','Linewidth',0.2)

%plot outer track
plot(trackData(:,5),trackData(:,6),'color','k','Linewidth',0.2)

scatter(traj(1:length(velProf3),1),traj(1:length(velProf3),2),5,velProf3,'filled')

R = linspace(1,0,256);
G = linspace(0,1,256);
B = zeros(1,256);
map = [R' G' B'];
colormap(map);

colBar = colorbar('eastoutside');
colBar.Label.String = "Velocity [m/s]";
xlabel('X [m]');
ylabel('Y [m]');
axis equal;
hold off;

xlabel('x(m)','fontweight','bold','fontsize',14)
ylabel('y(m)','fontweight','bold','fontsize',14)
title(sprintf('%s - Last-Lap Velocity Profile\nLap Time = %.2fs',name,time3(end)),'fontsize',16)
end