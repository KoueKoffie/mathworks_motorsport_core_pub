function [xp, yp, distance_c_XY] = pathPlanningDelaunay_f(x_i, y_i, x_o, y_o, interv)
%% Path Planning for Autonomous Race Cars Using Delaunay Triangulation
% The script demonstrates the steps to plan a path through a racing track. The 
% application is analogous to the first lap path planning of the Formula Student 
% Driverless competition. The goal is to plan the path based on detected cones 
% kept on the sides of the track.
% 

% 
% Methodology
% 
% Assumption: 
%% 
% * It is assumed that the perception algorithm is in place to detect the cones. 
% Step 1: Load cone position data

innerConePosition = [x_i' y_i'];
outerConePosition = [x_o' y_o'];

% Step 2: Preprocess the data
% In this step, we merge the inner and outer coordinates with alternate values. 
% This is just an assumption to demonstrate the workflow.

[m,nc] = size(innerConePosition); % size of the inner/outer cone positions data
P = zeros(2*m,nc); % initiate a P matrix consisting of inner and outer coordinates
P(1:2:2*m,:) = innerConePosition;
P(2:2:2*m,:) = outerConePosition; % merge the inner and outer coordinates with alternate values
xp = []; % create an empty numeric xp vector to store the planned x coordinates after each iteration
yp = []; % create an empty numeric yp vector to store the planned y coordinates after each iteration
% Step 3: Create 2-D DelaunayTriangulation objects at every nth interval of cone positions

% interv = 1000; % interval 

for i = interv:interv:2*m   
    DT = delaunayTriangulation(P(((abs((i-1)-interv)):i),:)); % create Delaunay tringulation for abs((i-1)-interv)):i points
    Pl = DT.Points; % coordinates of abs((i-1)-interv)):i vertices
    Cl = DT.ConnectivityList; % triangulation connectivity matrix
    [mc,nc] = size(Pl); % size
 
%     figure(1) % plot delaunay triangulations
%     triplot(DT,'k')
%     grid on
%     ax = gca;
%     ax.GridColor = [0, 0, 0];  % [R, G, B]
%     xlabel('x(m)')
%     ylabel('y (m)')
%     set(gca,'Color','#EEEEEE')
%     title('Delaunay Triangulation')
%     hold on
% Step 4: Define constraints to remove exterior triangles 
% While performing triangulation, the coordinates of the inner or outer cones 
% might create triangles outside the boundary of the track. These need to be removed 
% as might generate outliers. 

    % inner and outer constraints when the interval is even

    if rem(interv,2) == 0
        cIn = [2 1;(1:2:mc-3)' (3:2:(mc))'; (mc-1) mc];
        cOut = [(2:2:(mc-2))' (4:2:mc)'];
     
    else

    % inner and outer constraints when the interval is odd

         cIn = [2 1;(1:2:mc-2)' (3:2:(mc))'; (mc-1) mc];
         cOut = [(2:2:(mc-2))' (4:2:mc)'];

    end

    C = [cIn;cOut]; % create a matrix connecting the constraint boundaries

% Step 5: Create Delaunay triangulation with constraints

    TR = delaunayTriangulation(Pl,C); % Delaunay triangulation with constraints
    TL = isInterior(TR); % logical values that indicate whether the triangles are inside the boounded region
    TC = TR.ConnectivityList(TL,:); % triangulation connectivity matrix
% Step 6: Arrange the rows of connectivity matrix based on ascending sum of rows and create triangulations
% This step ensures that the triangles are connected in progressive order

    [~,pt] = sort(sum(TC,2));
    TS = TC(pt,:); % connectivity matrix based on ascending sum of rows
    TO = triangulation(TS,Pl); % create triangulations based on sorted connectivity matrix

% Step 7: Find midpoints of the edges

    xPo = TO.Points(:,1);
    yPo = TO.Points(:,2);

    E = edges(TO); % triangulation edges
    iseven = rem(E, 2) == 0; % remove bounded edges
    Eeven = E(any(iseven,2),:);
    isodd = rem(Eeven,2) ~=0;
    Eodd = Eeven(any(isodd,2),:);

    xmp = ((xPo((Eodd(:,1))) + xPo((Eodd(:,2))))/2); % x coordinate midpoints
    ymp = ((yPo((Eodd(:,1))) + yPo((Eodd(:,2))))/2); % y coordinate midpoints
    Pmp = [xmp ymp]; % midpoint coordinates
% Step 8: Interpolate the midpoints based on distance calculation

    distancematrix = squareform(pdist(Pmp));
    distancesteps = zeros(length(Pmp)-1,1);

    for j = 2:length(Pmp)
        distancesteps(j-1,1) = distancematrix(j,j-1);
    end
    totalDistance = sum(distancesteps); % total distance travelled
    distbp = cumsum([0; distancesteps]); % distance for each waypoint
    gradbp = linspace(0,totalDistance,100);
    xq = interp1(distbp,xmp,gradbp,'spline'); % interpolate x coordinates
    yq = interp1(distbp,ymp,gradbp,'spline'); % interpolate y coordinates
    xp = [xp xq(1:end-1)]; % store obtained x midpoints after each iteration
    yp = [yp yq(1:end-1)]; % store obtained y midpoints after each iteration

    
% Plot results

%     figure(2)
%     % subplot 
%     pos1 = [0.1 0.15 0.5 0.7];
%     subplot('Position',pos1)
%     pathPlanPlot(innerConePosition,outerConePosition,P,DT,TO,xmp,ymp,cIn,cOut,xq,yq)
%     title(['Path planning based on Constrained Delaunay' newline ' Triangulation'])
% 
%     % subplot
%     pos2 = [0.7 0.15 0.25 0.7];
%     subplot('Position',pos2)
%     pathPlanPlot(innerConePosition,outerConePosition,P,DT,TO,xmp,ymp,cIn,cOut,xq,yq)
%     xlim([min(min(xPo(1:2:(mc-1)),xPo(2:2:mc))) max(max(xPo(1:2:(mc-1)),xPo(2:2:mc)))])
%     ylim([min(min(yPo(1:2:(mc-1)),yPo(2:2:mc))) max(max(yPo(1:2:(mc-1)),yPo(2:2:mc)))])
end
%%

% h = legend('yCone','bCone','start','midpoint','internal edges',...
%     'inner boundary','outer boundary','planned path');
% Output the concatenated planned path

% Pp = [xp' yp']; % concatenated planned path
distance_c_XY(1) = 0;
distance_c_XY(2:length(xp)) = cumsum(sqrt((abs(diff(xp))).^2+(abs(diff(yp))).^2))';
%% 
% _Copyright 2022 The MathWorks, Inc._
end