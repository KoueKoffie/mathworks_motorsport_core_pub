%% Data form

% INPUT DATA
% first point is repeated
% track data form = [centers,outerBounds,innerBounds]
% name = 'name_of_track'

% OUTPUT DATA
% trajSP - [x-coordinate of traj y-coordinate of traj]
% trackData - [x-ref y-ref xin yin xout yout]

function [trajMCP, trackData, xinb,yinb,aYaw_opt] = minCurvaturePathGenFunction(centers,outerBounds,innerBounds,name, offset1,offset2,KMZ_direction,Track_direction)
%% Processing  track data


% interpolate data to get finer curve with equal distances between each segment

% higher no. of segments causes trajectory to follow the reference line
nseg = 1500;
xt = centers(:,1);
yt = centers(:,2);

% normal direction for each vertex
dx = gradient(xt);
dy = gradient(yt);
dL = hypot(dx,dy);

% offset curve - anonymous function
xoff = @(a) -a*dy./dL + xt;
yoff = @(a)  a*dx./dL + yt;

% % plot reference line
% figure('Name','Track with bounds,inner-limit,outer-limit and center')
% plot(xt,yt,'g')
% hold on


% for i = 1:numel(xt)
    xin = innerBounds(:,1);      % get inner offset curve
    yin = innerBounds(:,2);

% create offset for inner bound **************
dxin = gradient(xin);
dyin = gradient(yin);
dLin = hypot(dxin,dyin);

xoffin = @(a) -a*dyin./dLin + xin;
yoffin = @(a)  a*dxin./dLin + yin;

% offset = [0.5 -0.5];              % offset value  make input
if KMZ_direction == 0

    for i = 1:numel(offset1)
        xinb = xoffin(offset1(i))';      % get offset curve
        yinb = yoffin(offset1(i))';  
    end

else
    for i = 1:numel(offset1)
        xinb = xoffin(-offset1(i))';      % get offset curve
        yinb = yoffin(-offset1(i))';  
    end
end
    
% ****************************************
    xout  = outerBounds(:,1);      % get outer offset curve
    yout  = outerBounds(:,2);

% create offset for outer bound **************
dxout = gradient(xout);
dyout = gradient(yout);
dLout = hypot(dxout,dyout);

xoffout = @(a) -a*dyout./dLout + xout;
yoffout = @(a)  a*dxout./dLout + yout;

% offset = [-0.5 0.5];              % offset value
if KMZ_direction == 0
   
    for i = 1:numel(offset2)
        xoutb = xoffout(offset2(i))';      % get offset curve
        youtb = yoffout(offset2(i))';  
    end

else
    for i = 1:numel(offset2)
        xoutb = xoffout(-offset2(i))';      % get offset curve
        youtb = yoffout(-offset2(i))';  
    end
end
% ****************************************
% end

% % plot inner track
% plot(xin,yin,'color','b','linew',2)
% %plot inner bound track
% plot(xinb,yinb,'color','k','linew',2)
% % plot outer track
% plot(xout,yout,'color','r','linew',2)
% %plot outer bound track
% plot(xoutb,youtb,'color','k','linew',2)
% hold off
% axis equal
% 
% xlabel('x(m)','fontweight','bold','fontsize',14)
% ylabel('y(m)','fontweight','bold','fontsize',14)
% title(sprintf(name),'fontsize',16)
% 
% xlabel('x(m)','fontweight','bold','fontsize',14)
% ylabel('y(m)','fontweight','bold','fontsize',14)
% title(sprintf(name),'fontsize',16)


% form delta matrices
xoutb = xoutb';
youtb = youtb';
xinb = xinb';
yinb = yinb';
delx = xoutb - xinb;
dely = youtb - yinb;

trackData = [xt yt xinb yinb xoutb youtb];

%% Matrix Definition 

% number of segments
n = numel(delx);

% preallocation
H = zeros(n);
B = zeros(size(delx)).';

% formation of H matrix (nxn)
for i=2:n-1
    
    % first row
    H(i-1,i-1) = H(i-1,i-1) + delx(i-1)^2         + dely(i-1)^2;
    H(i-1,i)   = H(i-1,i)   - 2*delx(i-1)*delx(i) - 2*dely(i-1)*dely(i);
    H(i-1,i+1) = H(i-1,i+1) + delx(i-1)*delx(i+1) + dely(i-1)*dely(i+1);
    
    %second row
    H(i,i-1)   = H(i,i-1)   - 2*delx(i-1)*delx(i) - 2*dely(i-1)*dely(i);
    H(i,i)     = H(i,i )    + 4*delx(i)^2         + 4*dely(i)^2;
    H(i,i+1)   = H(i,i+1)   - 2*delx(i)*delx(i+1) - 2*dely(i)*dely(i+1);
    
    % third row
    H(i+1,i-1) = H(i+1,i-1) + delx(i-1)*delx(i+1) + dely(i-1)*dely(i+1);
    H(i+1,i)   = H(i+1,i)   - 2*delx(i)*delx(i+1) - 2*dely(i)*dely(i+1);
    H(i+1,i+1) = H(i+1,i+1) + delx(i+1)^2         + dely(i+1)^2;
    
end

% formation of B matrix (1xn)
for i=2:n-1
    
    B(1,i-1) = B(1,i-1) + 2*(xinb(i+1)+xinb(i-1)-2*xinb(i))*delx(i-1) + 2*(yinb(i+1)+yinb(i-1)-2*yinb(i))*dely(i-1);
    B(1,i)   = B(1,i) - 4*(xinb(i+1)+xinb(i-1)-2*xinb(i))*delx(i) - 4*(yinb(i+1)+yinb(i-1)-2*yinb(i))*dely(i);
    B(1,i+1) = B(1,i+1) + 2*(xinb(i+1)+xinb(i-1)-2*xinb(i))*delx(i+1) + 2*(yinb(i+1)+yinb(i-1)-2*yinb(i))*dely(i+1);
    
end

% define constraints
lb = zeros(n,1);
ub = ones(size(lb));

% if start and end points are the same
Aeq      =   zeros(1,n);
Aeq(1)   =   1;
Aeq(end) =   -1;
beq      =   0;
    
%% Solver

options = optimoptions('quadprog','Display','iter');
[resMCP,fval,exitflag,output] = quadprog(2*H,B',[],[],Aeq,beq,lb,ub,[],options);

%% Plotting results

% co-ordinates for the resultant curve
xresMCP = zeros(size(xt));
yresMCP = zeros(size(xt));

for i = 1:numel(xt)
    xresMCP(i) = xinb(i)+resMCP(i)*delx(i);
    yresMCP(i) = yinb(i)+resMCP(i)*dely(i);
end

% plot minimum curvature trajectory
figure
plot(xresMCP,yresMCP,'color','r','linew',2)
hold on

% plot starting line
plot([xinb(i) xoutb(i)], [yinb(i) youtb(i)],'color','green','linew',2)

% plot reference line
plot(xt,yt,'--')
hold on

% plot inner track
plot(xin,yin,'b')
hold on
plot(xinb,yinb,'k:')

%plot outer track
plot(xout,yout,'b')
hold on
plot(xoutb,youtb,'k:')

hold off
axis equal

xlabel('x(m)','fontweight','bold','fontsize',14)
ylabel('y(m)','fontweight','bold','fontsize',14)
title(sprintf(name,'%s - Minimum Curvature Trajectory'),'fontsize',16)

trajMCP = [xresMCP yresMCP];

yaw_interval = 1;
% aYaw_opt = unwrap(atan2(circshift(yresMCP,-yaw_interval)-circshift(yresMCP,+yaw_interval),circshift(xresMCP,-yaw_interval)-circshift(xresMCP,+yaw_interval)));
aYaw_opt = (atan2(circshift(yresMCP,-yaw_interval)-circshift(yresMCP,+yaw_interval),circshift(xresMCP,-yaw_interval)-circshift(xresMCP,+yaw_interval)));

end
