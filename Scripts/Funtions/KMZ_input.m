function [longitude_o, latitude_o, distance_o, longitude_i, latitude_i, distance_i, LL_lat, RU_lat, LL_lon, RU_lon] = KMZ_input(filename_i, filename_o, buffer_ratio,fliped)
%% 
% This is used to get track information from a KMZ file of the inner and outer 
% tracks of the Racetrack you want to find the optimal path for

inner = kmz2struct(filename_i);
outer = kmz2struct(filename_o);

longitude_o = outer.Lon';
latitude_o = outer.Lat';
if fliped == 1
    longitude_o = flip(longitude_o);
    latitude_o = flip(latitude_o);
end    
altitude_o = zeros(1,length(outer.Lat))';
origin_o = [latitude_o(1),longitude_o(1),0]; % origin used to estimate distance

longitude_i = inner.Lon';
latitude_i = inner.Lat';
if fliped == 1
    longitude_i = flip(longitude_i);
    latitude_i = flip(latitude_i);
end
altitude_i = zeros(1,length(inner.Lat))';
origin_i = [latitude_i(1),longitude_i(1),0]; % origin used to estimate distance

[x_i,y_i,~] = latlon2local(latitude_i,longitude_i,altitude_i,origin_i);
[x_o,y_o,~] = latlon2local(latitude_o,longitude_o,altitude_o,origin_o);

distance_i(1) = 0; 
distance_o(1) = 0; 
distance_i(2:length(latitude_i)) = cumsum(sqrt((abs(diff(x_i))).^2+(abs(diff(y_i))).^2));
distance_o(2:length(latitude_o)) = cumsum(sqrt((abs(diff(x_o))).^2+(abs(diff(y_o))).^2));

LL_lon = outer.BoundingBox(1,1);
LL_lat = outer.BoundingBox(1,2);
RU_lon = outer.BoundingBox(2,1);
RU_lat = outer.BoundingBox(2,2);

lat_d =  RU_lat-LL_lat;
lon_d = RU_lon-LL_lon;

LL_lat = LL_lat - buffer_ratio*(lat_d);
RU_lat = RU_lat + buffer_ratio*(lat_d);
LL_lon = LL_lon - buffer_ratio*(lon_d);
RU_lon = RU_lon + buffer_ratio*(lon_d);

end