function [lat,lon,ele] = opentopodata_surfread(lat_lookup, lon_lookup_s, lon_lookup_e, Dataset, N)

W_options = weboptions("Timeout",20);
httpsUrl = "https://api.opentopodata.org/";
lat_str = num2str(lat_lookup,15);
lon_str_s = num2str(lon_lookup_s,15);
lon_str_e = num2str(lon_lookup_e,15);
N_str = num2str(N,6);


dataUrl = strcat(httpsUrl, "v1/", Dataset,"?locations=",lat_str,",",lon_str_s,"|",lat_str,",",lon_str_e,"&samples=",N_str);
elevationFile = "TEMP.csv";
elevationFileFullPath = websave(elevationFile, dataUrl,W_options);
pause(0.01)
latlonele = readElevationSurf('TEMP.csv');
ele = latlonele(1:3:end);
lat = latlonele(2:3:end);
lon = latlonele(3:3:end);
end