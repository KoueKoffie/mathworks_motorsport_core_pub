function popup_dia(name,filename)
g = groot;
% v = get(g,'MonitorPositions')
v = get(g,'Screensize'); 
border_width = floor(v(3)/8.533);
font_size = floor(v(3)/170.6666666667);
left = v(1)+border_width;
bottom = v(2)+border_width;
width = v(3)-border_width*4.5;
height = v(4)-border_width*3;

d = dialog('Position',[left bottom width height],'Name','Manual Step');
message = sprintf('%s','Manual Step - to access DEM data from external source',newline,'Use https://www.gpsvisualizer.com/ to determine the elevation ',newline,'at each of the of the data points Geo_lat & Geo_lon',newline ,newline ,'Step 1: Go to https://www.gpsvisualizer.com/convert_input',newline,'Step 2: Choose file - browse to: ',filename,newline,'Step 3: Choose "best availiable source" under the Add DEM Elevation data',newline,'Step 4: Click Convert button and then save the resulting file',newline,'Step 5: Copy the file to the Output directory and rename to ',name,'_ele.txt',newline,newline,'Press continue once one the file is present in the directory'); 

    txt = uicontrol('Parent',d,'Style','text','HorizontalAlignment','left','FontSize',font_size,'Position',[border_width/2 border_width/2 width-border_width height-border_width],'String',message);
    btn = uicontrol('Parent',d,'Position',[width/2 border_width/4 70 25],'String','Continue','Callback','delete(gcf)');
    waitfor(d)  
end