%% Copyright 2021-2022 The MathWorks, Inc.
% Add shutdown commands here

% save Data/Recording_best.mat Recording_best
try
save_system("MathWorks_Motorsport_SSMBunreal_STLKyalami")
catch
    warning('model "MathWorks_Motorsport_SSMBunreal_STLKyalami" already closed')
end
try
save_system("MathWorks_Motorsport_SSMBunreal")
catch
    warning('model "MathWorks_Motorsport_SSMBunreal" already closed')
end
bdclose("all")