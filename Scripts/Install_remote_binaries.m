%% Version checking and Get remote Binaries

tic

% https://drive.google.com/file/d/1cVB2wfIqaF0TEaya_87-c_MCK6fRs3vl/view?usp=share_link

% warning settings
warning('off','backtrace')

% https://drive.google.com/file/d/1cVB2wfIqaF0TEaya_87-c_MCK6fRs3vl/view?usp=sharing


% Download Remote Version number
url = 'https://www.googleapis.com/drive/v3/files/1EPWuFsLEh-3QPabCCQHA7tswRRzkp1X4?alt=media&key=AIzaSyDNFL4iS5CW3d68Jvy6V8Rj0FaLmakUgUI';
websave('MM_B_Remote_version.mat',url);
load MM_B_Remote_version.mat

% Check if this is a first run - and download Remote Binaries if true
try
    load MM_B_Local_version.mat
catch
     warning('First run of this file - will download Binaries from remote source');
     % Download Binaries
     url = 'https://www.googleapis.com/drive/v3/files/1cVB2wfIqaF0TEaya_87-c_MCK6fRs3vl?alt=media&key=AIzaSyDNFL4iS5CW3d68Jvy6V8Rj0FaLmakUgUI';
     warning('First run of this file - will download Binaries from remote source');
     websave('MathWorks_Motorsport_Binaries.zip',url);
     % extract
     system('powershell -command Expand-Archive MathWorks_Motorsport_Binaries.zip')
     movefile MathWorks_Motorsport_Binaries\Compiled_Unreal_EXE\*.* Compiled_Unreal_EXE 
     movefile MathWorks_Motorsport_Binaries\Documentation\*.* Documentation
     movefile MathWorks_Motorsport_Binaries\Promo_video\*.* Promo_video
     movefile MathWorks_Motorsport_Binaries\RoadRunnerProject\*.* RoadRunnerProject
     movefile MathWorks_Motorsport_Binaries\UnrealProject\*.* UnrealProject
     rmdir('MathWorks_Motorsport_Binaries\','s')
     MM_B_Local_version = MM_B_Remote_version;
     save("MM_B_Local_version.mat","MM_B_Local_version")
     warning('Completed Downloading the remote Binaries');     
     try
        delete 'MathWorks_Motorsport_Binaries.zip'
     catch
        warning('MathWorks_Motorsport_Binaries.zip not found')
     end
end

% Check version
if MM_B_Remote_version == MM_B_Local_version
   warning('Local Version of binaries is up to date')
elseif MM_B_Remote_version > MM_B_Local_version
        warning('Local version of binaries is outdated new version is now being downloaded')
            % Download Binaries
            url = 'https://www.googleapis.com/drive/v3/files/1cVB2wfIqaF0TEaya_87-c_MCK6fRs3vl?alt=media&key=AIzaSyDNFL4iS5CW3d68Jvy6V8Rj0FaLmakUgUI';
            websave('MathWorks_Motorsport_Binaries.zip',url);
            % extract
            system('powershell -command Expand-Archive MathWorks_Motorsport_Binaries.zip')
            movefile MathWorks_Motorsport_Binaries\Compiled_Unreal_EXE\*.* Compiled_Unreal_EXE 
            movefile MathWorks_Motorsport_Binaries\Documentation\*.* Documentation
            movefile MathWorks_Motorsport_Binaries\Promo_video\*.* Promo_video
            movefile MathWorks_Motorsport_Binaries\RoadRunnerProject\*.* RoadRunnerProject
            movefile MathWorks_Motorsport_Binaries\UnrealProject\*.* UnrealProject
            rmdir('MathWorks_Motorsport_Binaries\','s')
            MM_B_Local_version = MM_B_Remote_version;
            save("MM_B_Local_version.mat","MM_B_Local_version")
            warning('Completed Downloading the remote Binaries');
            try
                delete 'MathWorks_Motorsport_Binaries.zip'
            catch
                warning('MathWorks_Motorsport_Binaries.zip not found')
            end
else
    warning('Local version of binaries is ahead of online version please push changes')
end
toc