%% Version checking and Get remote Binaries

tic

% warning settings
warning('off','backtrace')
% Download Remote Version number
url = 'https://www.googleapis.com/drive/v3/files/1EPWuFsLEh-3QPabCCQHA7tswRRzkp1X4?alt=media&key=AIzaSyDNFL4iS5CW3d68Jvy6V8Rj0FaLmakUgUI';
websave('MM_B_Remote_version.mat',url);
load MM_B_Remote_version.mat

            % move files
            movefile MathWorks_Motorsport_Binaries\Compiled_Unreal_EXE\*.* Compiled_Unreal_EXE 
            movefile MathWorks_Motorsport_Binaries\Documentation\*.* Documentation
            movefile MathWorks_Motorsport_Binaries\Promo_video\*.* Promo_video
            movefile MathWorks_Motorsport_Binaries\RoadRunnerProject\*.* RoadRunnerProject
            movefile MathWorks_Motorsport_Binaries\UnrealProject\*.* UnrealProject
            rmdir('MathWorks_Motorsport_Binaries\','s')
            MM_B_Local_version = MM_B_Remote_version;
            save("MM_B_Local_version.mat","MM_B_Local_version")
            warning('Completed installing the remote Binaries');
            try
            delete 'MathWorks_Motorsport_Binaries.zip'
            catch
                warning('MathWorks_Motorsport_Binaries.zip not found')
            end
toc