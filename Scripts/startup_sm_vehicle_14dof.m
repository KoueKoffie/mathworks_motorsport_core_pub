%% Copyright 2021-2022 The MathWorks, Inc.

% load Kyalami_Terrain_slow2.mat
% load Kyalami_Track_slow2.mat

% load Kyalami_Terrain_fast.mat
% load Kyalami_Track_fast.mat
load Camera_bak.mat
% load Recorded_Laps.mat

% Stiffness and Gravity factor
SM = 4/1;
GM = 3/1;

sm_vehicle_2axle_heave_roll_param
sm_vehicle_2axle_heave_roll_scene
% sm_vehicle_2axle_heave_roll_config_maneuver(bdroot,VehicleData,'stepsteer');

% Create Vehicle and event data
% Defaults for vehicle initial position in World coordinate frame
InitVehicle.Vehicle.px = Track.unreal.xyz_opt.Data(1,1);  % m
InitVehicle.Vehicle.py = -Track.unreal.xyz_opt.Data(1,2);  % m
InitVehicle.Vehicle.pz = -Track.unreal.xyz_opt.Data(1,3);  % m

% Defaults for vehicle initial translational velocity
% Represented in vehicle coordinates: 
%    +vx is forward, +vy is left, +vz is up in initial vehicle frame
InitVehicle.Vehicle.vx  =  0; %m/s
InitVehicle.Vehicle.vy  =  0;  %m/s
InitVehicle.Vehicle.vz  =  0;  %m/s

% Defaults for vehicle initial orientation
% Represented in vehicle coordinates, yaw-pitch-roll applied intrinsically
InitVehicle.Vehicle.yaw   = Track.unreal.yaw_opt.Data(1,1)-0.5*pi;  % rad
InitVehicle.Vehicle.pitch = 0;  % rad
InitVehicle.Vehicle.roll  = 0;  % rad

% Default is flat road surface in x-y plane
% SceneData = evalin('base','SceneData');
SceneData.Reference.yaw   = 0 * pi/180;
SceneData.Reference.pitch = 0 * pi/180;
SceneData.Reference.roll  = 0 * pi/180;

InitVehicle.Wheel.wFL = InitVehicle.Vehicle.vx/VehicleData.TireDataF.param.DIMENSION.UNLOADED_RADIUS; %rad/s
InitVehicle.Wheel.wFR = InitVehicle.Vehicle.vx/VehicleData.TireDataF.param.DIMENSION.UNLOADED_RADIUS; %rad/s
InitVehicle.Wheel.wRL = InitVehicle.Vehicle.vx/VehicleData.TireDataR.param.DIMENSION.UNLOADED_RADIUS; %rad/s
InitVehicle.Wheel.wRR = InitVehicle.Vehicle.vx/VehicleData.TireDataR.param.DIMENSION.UNLOADED_RADIUS; %rad/s



% Add MF-Swift software to path if it can be found
if(exist('sm_vehicle_startupMFSwift.m','file'))
    [~,MFSwifttbx_folders]=sm_vehicle_startupMFSwift;
    assignin('base','MFSwifttbx_folders',MFSwifttbx_folders);
end



% Open model and set the path to the Compiled Unreal Executable
MathWorks_Motorsport_SSMBunreal
Path2UnrealEXE = which('AutoVrtlEnv.exe');
blockpath = 'MathWorks_Motorsport_SSMBunreal/Unreal Visualization/Scene and Vehicle Configuration/Simulation 3D Scene Configuration';
set_param(blockpath,'ProjectName',Path2UnrealEXE)

% Open model and set the path to the Compiled Unreal Executable
MathWorks_Motorsport_SSMBunreal_STLKyalami
% Path2UnrealEXE = which('AutoVrtlEnv.exe');
blockpath = 'MathWorks_Motorsport_SSMBunreal_STLKyalami/Unreal Visualization/Scene and Vehicle Configuration/Simulation 3D Scene Configuration';
set_param(blockpath,'ProjectName',Path2UnrealEXE)


